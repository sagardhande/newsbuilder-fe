<?php 
function add_news_function() {
global $wpdb;
?>

<div class="wrap">
<h2>Add New Article</h2>
<div id="poststuff"><div id="post-body">

<div class="postbox">

<form method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>">

<h3 class="hndle"><label for="title">Article Source </label></h3>

<table width="100%" >
<tr>
<td width="25%">
<div class="inside">
<strong>Source</strong>
<br />               
<select id="date" name="news_rules_list" style="width:100%;">
<option value="abc-news" selected="">ABC News</option>
<option value="abc-news-au">ABC News (AU)</option>
<option value="aftenposten">Aftenposten</option>
<option value="al-jazeera-english">Al Jazeera English</option>
<option value="ansa">ANSA.it</option>
<option value="argaam">Argaam</option>
<option value="ars-technica">Ars Technica</option>
<option value="ary-news">Ary News</option>
<option value="associated-press">Associated Press</option>
<option value="australian-financial-review">Australian Financial Review</option>
<option value="axios">Axios</option>
<option value="bbc-news">BBC News</option>
<option value="bbc-sport">BBC Sport</option>
<option value="bild">Bild</option>
<option value="blasting-news-br">Blasting News (BR)</option>
<option value="bleacher-report">Bleacher Report</option>
<option value="bloomberg">Bloomberg</option>
<option value="breitbart-news">Breitbart News</option>
<option value="business-insider">Business Insider</option>
<option value="business-insider-uk">Business Insider (UK)</option>
<option value="buzzfeed">Buzzfeed</option>
<option value="cbc-news">CBC News</option>
<option value="cbs-news">CBS News</option>
<option value="cnbc">CNBC</option>
<option value="cnn">CNN</option>
<option value="cnn-es">CNN Spanish</option>
<option value="crypto-coins-news">Crypto Coins News</option>
<option value="daily-mail">Daily Mail</option>
<option value="der-tagesspiegel">Der Tagesspiegel</option>
<option value="die-zeit">Die Zeit</option>
<option value="el-mundo">El Mundo</option>
<option value="engadget">Engadget</option>
<option value="entertainment-weekly">Entertainment Weekly</option>
<option value="espn">ESPN</option>
<option value="espn-cric-info">ESPN Cric Info</option>
<option value="financial-post">Financial Post</option>
<option value="financial-times">Financial Times</option>
<option value="focus">Focus</option>
<option value="football-italia">Football Italia</option>
<option value="fortune">Fortune</option>
<option value="four-four-two">FourFourTwo</option>
<option value="fox-news">Fox News</option>
<option value="fox-sports">Fox Sports</option>
<option value="globo">Globo</option>
<option value="google-news">Google News</option>
<option value="google-news-ar">Google News (Argentina)</option>
<option value="google-news-au">Google News (Australia)</option>
<option value="google-news-br">Google News (Brasil)</option>
<option value="google-news-ca">Google News (Canada)</option>
<option value="google-news-fr">Google News (France)</option>
<option value="google-news-in">Google News (India)</option>
<option value="google-news-is">Google News (Israel)</option>
<option value="google-news-it">Google News (Italy)</option>
<option value="google-news-ru">Google News (Russia)</option>
<option value="google-news-sa">Google News (Saudi Arabia)</option>
<option value="google-news-uk">Google News (UK)</option>
<option value="goteborgs-posten">Göteborgs-Posten</option>
<option value="gruenderszene">Gruenderszene</option>
<option value="hacker-news">Hacker News</option>
<option value="handelsblatt">Handelsblatt</option>
<option value="ign">IGN</option>
<option value="il-sole-24-ore">Il Sole 24 Ore</option>
<option value="independent">Independent</option>
<option value="infobae">Infobae</option>
<option value="info-money">InfoMoney</option>
<option value="la-gaceta">La Gaceta</option>
<option value="la-nacion">La Nacion</option>
<option value="la-repubblica">La Repubblica</option>
<option value="le-monde">Le Monde</option>
<option value="lenta">Lenta</option>
<option value="lequipe">L'equipe</option>
<option value="les-echos">Les Echos</option>
<option value="liberation">Libération</option>
<option value="marca">Marca</option>
<option value="mashable">Mashable</option>
<option value="medical-news-today">Medical News Today</option>
<option value="metro">Metro</option>
<option value="mirror">Mirror</option>
<option value="msnbc">MSNBC</option>
<option value="mtv-news">MTV News</option>
<option value="mtv-news-uk">MTV News (UK)</option>
<option value="national-geographic">National Geographic</option>
<option value="national-review">National Review</option>
<option value="nbc-news">NBC News</option>
<option value="news24">News24</option>
<option value="new-scientist">New Scientist</option>
<option value="news-com-au">News.com.au</option>
<option value="newsweek">Newsweek</option>
<option value="new-york-magazine">New York Magazine</option>
<option value="next-big-future">Next Big Future</option>
<option value="nfl-news">NFL News</option>
<option value="nhl-news">NHL News</option>
<option value="nrk">NRK</option>
<option value="politico">Politico</option>
<option value="polygon">Polygon</option>
<option value="rbc">RBC</option>
<option value="recode">Recode</option>
<option value="reddit-r-all">Reddit /r/all</option>
<option value="reuters">Reuters</option>
<option value="rt">RT</option>
<option value="rte">RTE</option>
<option value="rtl-nieuws">RTL Nieuws</option>
<option value="sabq">SABQ</option>
<option value="spiegel-online">Spiegel Online</option>
<option value="svenska-dagbladet">Svenska Dagbladet</option><option value="t3n">T3n</option><option value="talksport">TalkSport</option>
<option value="techcrunch">TechCrunch</option>
<option value="techcrunch-cn">TechCrunch (CN)</option><option value="techradar">TechRadar</option><option value="the-american-conservative">The American Conservative</option>
<option value="the-economist">The Economist</option>
<option value="the-globe-and-mail">The Globe And Mail</option>
<option value="the-guardian-au">The Guardian (AU)</option>
<option value="the-guardian-uk">The Guardian (UK)</option>
<option value="the-hill">The Hill</option>
<option value="the-hindu">The Hindu</option>
<option value="the-huffington-post">The Huffington Post</option><option value="the-irish-times">The Irish Times</option>
<option value="the-jerusalem-post">The Jerusalem Post</option>
<option value="the-lad-bible">The Lad Bible</option>
<option value="the-new-york-times">The New York Times</option>
<option value="the-next-web">The Next Web</option>
<option value="the-sport-bible">The Sport Bible</option>
<option value="the-telegraph">The Telegraph</option>
<option value="the-times-of-india">The Times of India</option>
<option value="the-verge">The Verge</option><option value="the-wall-street-journal">The Wall Street Journal</option>
<option value="the-washington-post">The Washington Post</option>
<option value="the-washington-times">The Washington Times</option>
<option value="time">Time</option>
<option value="usa-today">USA Today</option>
<option value="vice-news">Vice News</option>
<option value="wired">Wired</option>
<option value="wired-de">Wired.de</option>
<option value="wirtschafts-woche">Wirtschafts Woche</option>
<option value="xinhua-net">Xinhua Net</option>
<option value="ynet">Ynet</option>
<option value="any">Any</option>
</select>          
</div>
</td>

<td width="10%">
<div class="inside">
<strong>Source Category </strong>
<br />               
<select id="date" name="news_rules_category" style="width:100%;">
	<option value="">Please select</option>
	<option value="business">Business</option>
	<option value="entertainment">Entertainment</option>
	<option value="general">General</option>
	<option value="health">health</option>
	<option value="science">Science</option>
	<option value="sports">Sports</option>
	<option value="technology">Technology</option>
</select>      
</div>
</td>


<?php 
$categories = get_categories();
?>


<td width="10%">
<div class="inside">
<strong>Country </strong>
<br />               
<select id="post_sel_country" name="post_sel_country" style="width:100%;">
<option value="" selected="">Please Select</option>
<option value="ar">Argentina</option>
<option value="au">Australia</option>
<option value="at">Austria</option>
<option value="be">Belgium</option>
<option value="br">Brazil</option>
<option value="bg">Bulgaria</option>
<option value="ca">Canada</option>
<option value="cn">China</option>
<option value="co">Colombia</option>
<option value="cu">Cuba</option>
<option value="cz">Czech Republic</option>
<option value="eg">Egypt</option>
<option value="fr">France</option>
<option value="de">Germany</option>
<option value="gr">Greece</option>
<option value="hk">Hong Kong</option>
<option value="hu">Hungary</option>
<option value="in">India</option>
<option value="id">Indonesia</option>
<option value="ie">Ireland</option>
<option value="il">Israel</option>
<option value="be">Belgium</option>
<option value="it">Italy</option>
<option value="jp">Japan</option>
<option value="lv">Latvia</option>
<option value="lt">Lithuania</option>
<option value="my">Malaysia</option>
<option value="mx">Mexico</option>
<option value="ma">Morocco</option>
<option value="nl">Netherlands</option>
<option value="nz">New Zealand</option>
<option value="ng">Nigeria</option>
<option value="no">Norway</option>
<option value="ph">Philippines</option>
<option value="pl">Poland</option>
<option value="pt">Portugal</option>
<option value="ro">Romania</option>
<option value="ru">Russia</option>
<option value="sa">Saudi Arabia</option>
<option value="rs">Serbia</option>
<option value="sg">Singapore</option>
<option value="sk">Slovakia</option>
<option value="za">South Africa</option>
<option value="kr">South Korea</option>
<option value="se">Sweden</option>
<option value="ch">Switzerland</option>
<option value="pl">Poland</option>
<option value="tw">Taiwan</option>
<option value="th">Thailand</option>
<option value="tr">Turkey</option>
<option value="ae">UAE</option>
<option value="ua">Ukraine</option>
<option value="gb">United Kingdom</option>
<option value="us">United States</option>
<option value="ve">Venuzuela</option>
</select>    
</div>
</td>


<td width="10%">
<div class="inside">
<strong>Max # Posts </strong>
<br />               
<input type="number" step="1" min="0" placeholder="# max" max="100" name="no_post" style="width:100px;" value="10" required="">       
</div>
</td>



<td width="10%">
<div class="inside">
<strong>Post Status </strong>
<br />               
<select id="post_status" name="post_status" style="width:100%;">
<option value="pending">Moderate -&gt; Pending</option>
<option value="draft">Moderate -&gt; Draft</option>
<option value="publish" selected="">Published</option>
<option value="private">Private</option>
<option value="trash">Trash</option>
</select>    
</div>
</td>

<td width="10%">
<div class="inside">
<strong>Item Type </strong>
<br />               
<select id="post_sel_type" name="post_sel_type" style="width:100%;">
<option value="post" selected="">post</option>
<option value="page">page</option>
<option value="attachment">attachment</option>
<option value="revision">revision</option>
<option value="nav_menu_item">nav_menu_item</option>

</select>    
</div>
</td>


<td width="10%">
<div class="inside">

<br />               
<input type="submit" name="slm_search_btn2" class="button button-primary" value="Save Article" />
<br />            
</div>
</td>

</tr>

<tr><td >
<div class="inside">
<strong>Search Articles by Keyword </strong>
<br />               
<input type="text" placeholder="Optional"  name="search_artical_title" style="width:300px;">       
</div>
</td>

<td width="15%" >
<div class="inside">
<strong>Wordpress Category</strong>
<br />               
<select id="date" name="news_rules_list_category" style="width:100%;">
<option disabled="" >--Default Category--</option>
<?php
foreach($categories as $category) {
?>
<option value="<?php echo $category->name;?>">
<?php echo $category->name;?>
</option>
<?php
}
?>
<option disabled="" >--New Category--</option>
<option selected="selected" value="Uncategorized">Uncategorized</option>
<option value="Business"> Business</option>
<option value="Entertainment"> Entertainment</option> 
<option value="Gaming"> Gaming</option> 
<option value="General"> General</option> 
<option value="Health-and-Medical"> Health and Medical</option> 
<option value="Music"> Music</option> 
<option value="Politics"> Politics</option> 
<option value="Science-and-Nature"> Science and Nature</option> 
<option value="Sport"> Sport</option> 
<option value="Technology"> Technology</option>


</select>          
</div>
</td>

<td width="10%">
<div class="inside">
<strong>Schedule(Cron) </strong>
<br />               
<select id="post_schedule" name="post_schedule" style="width:100%;">
<option value="virtual">Virtual(On Website Refresh)</option>    
<option value="real">Real(CPanel)</option>               
</select>

</div>
</td>

</tr>

</table>

</form>
</div> 

<?php 
$article_source_table = $wpdb->prefix . "article_source_tbl";
$results = $wpdb->get_results("SELECT * FROM $article_source_table where source_type='NEWS' ");

function objectToArray($d) {
if (is_object($d)) {  
$d = get_object_vars($d);
}

if (is_array($d)) {

return array_map(__FUNCTION__, $d);
}
else {
return $d;
}
}

$var1 = objectToArray($results);

?>
<div class="postbox">
<?php  
for($i=0;$i<count($var1);$i++)
{

echo $var1[$i]['post_schedule'];
?>
<form method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>">

<table  style="overflow-x:auto;width: 100%;">
<tr>
<td width="30%">

<div class="inside">
<strong>Article Source</strong>
<br />               
<select id="date" name="news_rules_list2" style="width:100%;">
<option  <?php if ($var1[$i]['source_name'] == "abc-news" ) echo 'selected' ; ?> value="abc-news">ABC News</option>
<option  <?php if ($var1[$i]['source_name'] == "abc-news-au" ) echo 'selected' ; ?> value="abc-news-au" >ABC News (AU)</option>
<option  <?php if ($var1[$i]['source_name'] == "aftenposten" ) echo 'selected' ; ?> value="aftenposten" >Aftenposten</option>
<option <?php if ($var1[$i]['source_name'] == "al-jazeera-english" ) echo 'selected' ; ?> value="al-jazeera-english" >Al Jazeera English</option>
<option <?php if ($var1[$i]['source_name'] == "ansa" ) echo 'selected' ; ?> value="ansa">ANSA.it</option>
<option <?php if ($var1[$i]['source_name'] == "argaam" ) echo 'selected' ; ?> value="argaam">Argaam</option>
<option <?php if ($var1[$i]['source_name'] == "ars-technica" ) echo 'selected' ; ?> value="ars-technica">Ars Technica</option>
<option <?php if ($var1[$i]['source_name'] == "ary-news" ) echo 'selected' ; ?> value="ary-news">Ary News</option>
<option <?php if ($var1[$i]['source_name'] == "associated-press" ) echo 'selected' ; ?> value="associated-press">Associated Press</option>
<option <?php if ($var1[$i]['source_name'] == "australian-financial-review" ) echo 'selected' ; ?> value="australian-financial-review">Australian Financial Review</option>
<option <?php if ($var1[$i]['source_name'] == "axios" ) echo 'selected' ; ?> value="axios">Axios</option>
<option <?php if ($var1[$i]['source_name'] == "bbc-news" ) echo 'selected' ; ?> value="bbc-news">BBC News</option>
<option <?php if ($var1[$i]['source_name'] == "bbc-sport" ) echo 'selected' ; ?> value="bbc-sport">BBC Sport</option>
<option <?php if ($var1[$i]['source_name'] == "bild" ) echo 'selected' ; ?> value="bild">Bild</option>
<option <?php if ($var1[$i]['source_name'] == "blasting-news-br" ) echo 'selected' ; ?> value="blasting-news-br">Blasting News (BR)</option>
<option <?php if ($var1[$i]['source_name'] == "bleacher-report" ) echo 'selected' ; ?> value="bleacher-report">Bleacher Report</option>
<option <?php if ($var1[$i]['source_name'] == "bloomberg" ) echo 'selected' ; ?> value="bloomberg">Bloomberg</option>
<option <?php if ($var1[$i]['source_name'] == "breitbart-news" ) echo 'selected' ; ?> value="breitbart-news">Breitbart News</option>
<option <?php if ($var1[$i]['source_name'] == "business-insider" ) echo 'selected' ; ?> value="business-insider">Business Insider</option>
<option <?php if ($var1[$i]['source_name'] == "business-insider-uk" ) echo 'selected' ; ?> value="business-insider-uk">Business Insider (UK)</option>
<option <?php if ($var1[$i]['source_name'] == "buzzfeed" ) echo 'selected' ; ?> value="buzzfeed">Buzzfeed</option>
<option <?php if ($var1[$i]['source_name'] == "cbc-news" ) echo 'selected' ; ?> value="cbc-news">CBC News</option>
<option <?php if ($var1[$i]['source_name'] == "cbs-news" ) echo 'selected' ; ?> value="cbs-news">CBS News</option>
<option <?php if ($var1[$i]['source_name'] == "cnbc" ) echo 'selected' ; ?> value="cnbc">CNBC</option>
<option <?php if ($var1[$i]['source_name'] == "cnn" ) echo 'selected' ; ?> value="cnn">CNN</option>
<option <?php if ($var1[$i]['source_name'] == "cnn-es" ) echo 'selected' ; ?> value="cnn-es">CNN Spanish</option>
<option <?php if ($var1[$i]['source_name'] == "crypto-coins-news" ) echo 'selected' ; ?> value="crypto-coins-news">Crypto Coins News</option>
<option <?php if ($var1[$i]['source_name'] == "daily-mail" ) echo 'selected' ; ?> value="daily-mail">Daily Mail</option>
<option <?php if ($var1[$i]['source_name'] == "der-tagesspiegel" ) echo 'selected' ; ?> value="der-tagesspiegel">Der Tagesspiegel</option>
<option <?php if ($var1[$i]['source_name'] == "die-zeit" ) echo 'selected' ; ?> value="die-zeit">Die Zeit</option>
<option <?php if ($var1[$i]['source_name'] == "el-mundo" ) echo 'selected' ; ?> value="el-mundo">El Mundo</option>
<option <?php if ($var1[$i]['source_name'] == "engadget" ) echo 'selected' ; ?> value="engadget">Engadget</option>
<option <?php if ($var1[$i]['source_name'] == "entertainment-weekly" ) echo 'selected' ; ?> value="entertainment-weekly">Entertainment Weekly</option>
<option <?php if ($var1[$i]['source_name'] == "espn" ) echo 'selected' ; ?> value="espn">ESPN</option>
<option <?php if ($var1[$i]['source_name'] == "espn-cric-info" ) echo 'selected' ; ?> value="espn-cric-info">ESPN Cric Info</option>
<option <?php if ($var1[$i]['source_name'] == "financial-post" ) echo 'selected' ; ?> value="financial-post">Financial Post</option>
<option <?php if ($var1[$i]['source_name'] == "financial-times" ) echo 'selected' ; ?> value="financial-times">Financial Times</option>
<option <?php if ($var1[$i]['source_name'] == "focus" ) echo 'selected' ; ?> value="focus">Focus</option>
<option <?php if ($var1[$i]['source_name'] == "football-italia" ) echo 'selected' ; ?> value="football-italia">Football Italia</option>
<option <?php if ($var1[$i]['source_name'] == "fortune" ) echo 'selected' ; ?> value="fortune">Fortune</option>
<option <?php if ($var1[$i]['source_name'] == "four-four-two" ) echo 'selected' ; ?> value="four-four-two">FourFourTwo</option>
<option <?php if ($var1[$i]['source_name'] == "fox-news" ) echo 'selected' ; ?> value="fox-news">Fox News</option>
<option <?php if ($var1[$i]['source_name'] == "fox-sports" ) echo 'selected' ; ?> value="fox-sports">Fox Sports</option>
<option <?php if ($var1[$i]['source_name'] == "globo" ) echo 'selected' ; ?> value="globo">Globo</option>
<option <?php if ($var1[$i]['source_name'] == "google-news" ) echo 'selected' ; ?> value="google-news">Google News</option>
<option <?php if ($var1[$i]['source_name'] == "google-news-ar" ) echo 'selected' ; ?> value="google-news-ar">Google News (Argentina)</option>
<option <?php if ($var1[$i]['source_name'] == "google-news-au" ) echo 'selected' ; ?> value="google-news-au">Google News (Australia)</option>
<option <?php if ($var1[$i]['source_name'] == "google-news-br" ) echo 'selected' ; ?> value="google-news-br">Google News (Brasil)</option>
<option <?php if ($var1[$i]['source_name'] == "google-news-ca" ) echo 'selected' ; ?> value="google-news-ca">Google News (Canada)</option>
<option <?php if ($var1[$i]['source_name'] == "google-news-fr" ) echo 'selected' ; ?> value="google-news-fr">Google News (France)</option>
<option <?php if ($var1[$i]['source_name'] == "google-news-in" ) echo 'selected' ; ?> value="google-news-in">Google News (India)</option>
<option <?php if ($var1[$i]['source_name'] == "google-news-is" ) echo 'selected' ; ?> value="google-news-is">Google News (Israel)</option>
<option <?php if ($var1[$i]['source_name'] == "google-news-it" ) echo 'selected' ; ?> value="google-news-it">Google News (Italy)</option>
<option <?php if ($var1[$i]['source_name'] == "google-news-ru" ) echo 'selected' ; ?> value="google-news-ru">Google News (Russia)</option>
<option <?php if ($var1[$i]['source_name'] == "google-news-sa" ) echo 'selected' ; ?> value="google-news-sa">Google News (Saudi Arabia)</option>
<option <?php if ($var1[$i]['source_name'] == "google-news-uk" ) echo 'selected' ; ?> value="google-news-uk">Google News (UK)</option>
<option <?php if ($var1[$i]['source_name'] == "goteborgs-posten" ) echo 'selected' ; ?> value="goteborgs-posten">Göteborgs-Posten</option>
<option <?php if ($var1[$i]['source_name'] == "gruenderszene" ) echo 'selected' ; ?> value="gruenderszene">Gruenderszene</option>
<option <?php if ($var1[$i]['source_name'] == "hacker-news" ) echo 'selected' ; ?> value="hacker-news">Hacker News</option>
<option <?php if ($var1[$i]['source_name'] == "handelsblatt" ) echo 'selected' ; ?> value="handelsblatt">Handelsblatt</option>
<option <?php if ($var1[$i]['source_name'] == "ign" ) echo 'selected' ; ?> value="ign">IGN</option>
<option <?php if ($var1[$i]['source_name'] == "il-sole-24-ore" ) echo 'selected' ; ?> value="il-sole-24-ore">Il Sole 24 Ore</option>
<option <?php if ($var1[$i]['source_name'] == "independent" ) echo 'selected' ; ?> value="independent">Independent</option>
<option <?php if ($var1[$i]['source_name'] == "infobae" ) echo 'selected' ; ?> value="infobae">Infobae</option>
<option <?php if ($var1[$i]['source_name'] == "info-money" ) echo 'selected' ; ?> value="info-money">InfoMoney</option>
<option <?php if ($var1[$i]['source_name'] == "la-gaceta" ) echo 'selected' ; ?> value="la-gaceta">La Gaceta</option>
<option <?php if ($var1[$i]['source_name'] == "la-nacion" ) echo 'selected' ; ?> value="la-nacion">La Nacion</option>
<option <?php if ($var1[$i]['source_name'] == "la-repubblica" ) echo 'selected' ; ?> value="la-repubblica">La Repubblica</option>
<option <?php if ($var1[$i]['source_name'] == "le-monde" ) echo 'selected' ; ?> value="le-monde">Le Monde</option>
<option <?php if ($var1[$i]['source_name'] == "lenta" ) echo 'selected' ; ?> value="lenta">Lenta</option>
<option <?php if ($var1[$i]['source_name'] == "lequipe" ) echo 'selected' ; ?> value="lequipe">L'equipe</option>
<option <?php if ($var1[$i]['source_name'] == "les-echos" ) echo 'selected' ; ?> value="les-echos">Les Echos</option>
<option <?php if ($var1[$i]['source_name'] == "liberation" ) echo 'selected' ; ?> value="liberation">Libération</option>
<option <?php if ($var1[$i]['source_name'] == "marca" ) echo 'selected' ; ?> value="marca">Marca</option>
<option <?php if ($var1[$i]['source_name'] == "mashable" ) echo 'selected' ; ?> value="mashable">Mashable</option>
<option <?php if ($var1[$i]['source_name'] == "medical-news-today" ) echo 'selected' ; ?> value="medical-news-today">Medical News Today</option>
<option <?php if ($var1[$i]['source_name'] == "metro" ) echo 'selected' ; ?> value="metro">Metro</option>
<option <?php if ($var1[$i]['source_name'] == "mirror" ) echo 'selected' ; ?> value="mirror">Mirror</option>
<option <?php if ($var1[$i]['source_name'] == "msnbc" ) echo 'selected' ; ?> value="msnbc">MSNBC</option>
<option <?php if ($var1[$i]['source_name'] == "mtv-news" ) echo 'selected' ; ?> value="mtv-news">MTV News</option>
<option <?php if ($var1[$i]['source_name'] == "mtv-news-uk" ) echo 'selected' ; ?> value="mtv-news-uk">MTV News (UK)</option>
<option <?php if ($var1[$i]['source_name'] == "national-geographic" ) echo 'selected' ; ?> value="national-geographic">National Geographic</option>
<option <?php if ($var1[$i]['source_name'] == "national-review" ) echo 'selected' ; ?> value="national-review">National Review</option>
<option <?php if ($var1[$i]['source_name'] == "nbc-news" ) echo 'selected' ; ?> value="nbc-news">NBC News</option>
<option <?php if ($var1[$i]['source_name'] == "news24" ) echo 'selected' ; ?> value="news24">News24</option>
<option <?php if ($var1[$i]['source_name'] == "new-scientist" ) echo 'selected' ; ?> value="new-scientist">New Scientist</option>
<option <?php if ($var1[$i]['source_name'] == "news-com-au" ) echo 'selected' ; ?> value="news-com-au">News.com.au</option>
<option <?php if ($var1[$i]['source_name'] == "newsweek" ) echo 'selected' ; ?> value="newsweek">Newsweek</option>
<option <?php if ($var1[$i]['source_name'] == "new-york-magazine" ) echo 'selected' ; ?> value="new-york-magazine">New York Magazine</option>
<option <?php if ($var1[$i]['source_name'] == "next-big-future" ) echo 'selected' ; ?> value="next-big-future">Next Big Future</option>
<option <?php if ($var1[$i]['source_name'] == "nfl-news" ) echo 'selected' ; ?> value="nfl-news">NFL News</option>
<option <?php if ($var1[$i]['source_name'] == "nhl-news" ) echo 'selected' ; ?> value="nhl-news">NHL News</option>
<option <?php if ($var1[$i]['source_name'] == "nrk" ) echo 'selected' ; ?> value="nrk">NRK</option>
<option <?php if ($var1[$i]['source_name'] == "politico" ) echo 'selected' ; ?> value="politico">Politico</option>
<option <?php if ($var1[$i]['source_name'] == "polygon" ) echo 'selected' ; ?> value="polygon">Polygon</option>
<option <?php if ($var1[$i]['source_name'] == "rbc" ) echo 'selected' ; ?> value="rbc">RBC</option>
<option <?php if ($var1[$i]['source_name'] == "recode" ) echo 'selected' ; ?> value="recode">Recode</option>
<option <?php if ($var1[$i]['source_name'] == "reddit-r-all" ) echo 'selected' ; ?> value="reddit-r-all">Reddit /r/all</option>
<option <?php if ($var1[$i]['source_name'] == "reuters" ) echo 'selected' ; ?> value="reuters">Reuters</option>
<option <?php if ($var1[$i]['source_name'] == "rt" ) echo 'selected' ; ?> value="rt">RT</option>
<option <?php if ($var1[$i]['source_name'] == "rte" ) echo 'selected' ; ?> value="rte">RTE</option>
<option <?php if ($var1[$i]['source_name'] == "rtl-nieuws" ) echo 'selected' ; ?> value="rtl-nieuws">RTL Nieuws</option>
<option <?php if ($var1[$i]['source_name'] == "sabq" ) echo 'selected' ; ?> value="sabq">SABQ</option>
<option <?php if ($var1[$i]['source_name'] == "spiegel-online" ) echo 'selected' ; ?> value="spiegel-online">Spiegel Online</option>
<option <?php if ($var1[$i]['source_name'] == "svenska-dagbladet" ) echo 'selected' ; ?> value="svenska-dagbladet">Svenska Dagbladet</option>
<option <?php if ($var1[$i]['source_name'] == "t3n" ) echo 'selected' ; ?> value="t3n">T3n</option>
<option <?php if ($var1[$i]['source_name'] == "talksport" ) echo 'selected' ; ?> value="talksport">TalkSport</option>
<option <?php if ($var1[$i]['source_name'] == "techcrunch" ) echo 'selected' ; ?> value="techcrunch">TechCrunch</option>
<option <?php if ($var1[$i]['source_name'] == "techcrunch-cn" ) echo 'selected' ; ?> value="techcrunch-cn">TechCrunch (CN)</option>
<option <?php if ($var1[$i]['source_name'] == "techradar" ) echo 'selected' ; ?> value="techradar">TechRadar</option>
<option <?php if ($var1[$i]['source_name'] == "the-american-conservative" ) echo 'selected' ; ?> value="the-american-conservative">The American Conservative</option>
<option <?php if ($var1[$i]['source_name'] == "the-economist" ) echo 'selected' ; ?> value="the-economist">The Economist</option>
<option <?php if ($var1[$i]['source_name'] == "the-globe-and-mail" ) echo 'selected' ; ?> value="the-globe-and-mail">The Globe And Mail</option>
<option <?php if ($var1[$i]['source_name'] == "the-guardian-au" ) echo 'selected' ; ?> value="the-guardian-au">The Guardian (AU)</option>
<option <?php if ($var1[$i]['source_name'] == "the-guardian-uk" ) echo 'selected' ; ?> value="the-guardian-uk">The Guardian (UK)</option>
<option <?php if ($var1[$i]['source_name'] == "the-hill" ) echo 'selected' ; ?> value="the-hill">The Hill</option>
<option <?php if ($var1[$i]['source_name'] == "the-hindu" ) echo 'selected' ; ?> value="the-hindu">The Hindu</option>
<option <?php if ($var1[$i]['source_name'] == "the-huffington-post" ) echo 'selected' ; ?> value="the-huffington-post">The Huffington Post</option>
<option <?php if ($var1[$i]['source_name'] == "the-irish-times" ) echo 'selected' ; ?> value="the-irish-times">The Irish Times</option>
<option <?php if ($var1[$i]['source_name'] == "the-jerusalem-post" ) echo 'selected' ; ?> value="the-jerusalem-post">The Jerusalem Post</option>
<option <?php if ($var1[$i]['source_name'] == "the-lad-bible" ) echo 'selected' ; ?> value="the-lad-bible">The Lad Bible</option>
<option <?php if ($var1[$i]['source_name'] == "the-new-york-times" ) echo 'selected' ; ?> value="the-new-york-times">The New York Times</option>
<option <?php if ($var1[$i]['source_name'] == "the-next-web" ) echo 'selected' ; ?> value="the-next-web">The Next Web</option>
<option <?php if ($var1[$i]['source_name'] == "the-sport-bible" ) echo 'selected' ; ?> value="the-sport-bible">The Sport Bible</option>
<option <?php if ($var1[$i]['source_name'] == "the-telegraph" ) echo 'selected' ; ?> value="the-telegraph">The Telegraph</option>
<option <?php if ($var1[$i]['source_name'] == "the-times-of-india" ) echo 'selected' ; ?> value="the-times-of-india">The Times of India</option>
<option <?php if ($var1[$i]['source_name'] == "the-verge" ) echo 'selected' ; ?> value="the-verge">The Verge</option>
<option <?php if ($var1[$i]['source_name'] == "the-wall-street-journal" ) echo 'selected' ; ?> value="the-wall-street-journal">The Wall Street Journal</option>
<option <?php if ($var1[$i]['source_name'] == "the-washington-post" ) echo 'selected' ; ?> value="the-washington-post">The Washington Post</option>
<option <?php if ($var1[$i]['source_name'] == "the-washington-times" ) echo 'selected' ; ?> value="the-washington-times">The Washington Times</option>
<option <?php if ($var1[$i]['source_name'] == "time" ) echo 'selected' ; ?> value="time">Time</option>
<option <?php if ($var1[$i]['source_name'] == "usa-today" ) echo 'selected' ; ?> value="usa-today">USA Today</option>
<option <?php if ($var1[$i]['source_name'] == "vice-news" ) echo 'selected' ; ?> value="vice-news">Vice News</option>
<option <?php if ($var1[$i]['source_name'] == "wired" ) echo 'selected' ; ?> value="wired">Wired</option>
<option <?php if ($var1[$i]['source_name'] == "wired-de" ) echo 'selected' ; ?> value="wired-de">Wired.de</option>
<option <?php if ($var1[$i]['source_name'] == "wirtschafts-woche" ) echo 'selected' ; ?> value="wirtschafts-woche">Wirtschafts Woche</option>
<option <?php if ($var1[$i]['source_name'] == "xinhua-net" ) echo 'selected' ; ?> value="xinhua-net">Xinhua Net</option>
<option <?php if ($var1[$i]['source_name'] == "ynet" ) echo 'selected' ; ?> value="ynet">Ynet</option>
<option <?php if ($var1[$i]['source_name'] == "any" ) echo 'selected' ; ?> value="any">Any</option>
</select>             

</div>
</td>

<td width="15%" >
<div class="inside">
<strong>Category</strong>
<br />               
<input type="text"  value="<?php echo $var1[$i]['source_category'];?>" name="news_rules_list_category2" style="width:120px;" >             
</div>
</td>


<td width="10%">
<div class="inside">
<strong>Max # Posts </strong>
<br />               
<input type="number" step="1" min="0" placeholder="# max" max="100" name="no_post" style="width:100px;" value="<?php echo $var1[$i]['no_post']; ?>" required="">       
</div>
</td>

<td width="10%">
<div class="inside">
<strong>Schedule </strong>
<br />               
<select id="post_schedule" name="post_schedule" style="width:150px;">
<option <?php if ($var1[$i]['schedule_post'] == "virtual" ) echo 'selected' ; ?> value="virtual">Virtual(On Website Refresh)</option>
<option <?php if ($var1[$i]['schedule_post'] == "real" ) echo 'selected' ; ?> value="real">Real(CPanel)</option>             
</select>              
</div>
</td>

<td width="10%">
<div class="inside">
<strong>Post Status </strong>
<br />               
<select id="post_status" name="post_status" style="width:100px;">
<option <?php if ($var1[$i]['post_status'] == "pending" ) echo 'selected' ; ?> value="pending">Moderate -&gt; Pending</option>
<option <?php if ($var1[$i]['post_status'] == "draft" ) echo 'selected' ; ?> value="draft">Moderate -&gt; Draft</option>
<option <?php if ($var1[$i]['post_status'] == "publish" ) echo 'selected' ; ?> value="publish" selected="">Published</option>
<option <?php if ($var1[$i]['post_status'] == "private" ) echo 'selected' ; ?> value="private">Private</option>
<option <?php if ($var1[$i]['post_status'] == "trash" ) echo 'selected' ; ?> value="trash">Trash</option>
</select>    
</div>
</td>

<td width="10%">
<div class="inside">
<strong>Item Type </strong>
<br />               
<select id="post_sel_type" name="post_sel_type" style="width:100px;">
<option <?php if ($var1[$i]['post_sel_type'] == "post" ) echo 'selected' ; ?> value="post" selected="">post</option>
<option <?php if ($var1[$i]['post_sel_type'] == "page" ) echo 'selected' ; ?> value="page">page</option>
<option <?php if ($var1[$i]['post_sel_type'] == "attachment" ) echo 'selected' ; ?> value="attachment">attachment</option>
<option <?php if ($var1[$i]['post_sel_type'] == "revision" ) echo 'selected' ; ?> value="revision">revision</option>
<option <?php if ($var1[$i]['post_sel_type'] == "nav_menu_item" ) echo 'selected' ; ?> value="nav_menu_item">nav_menu_item</option>

</select>    
</div>
</td>

<td width="10%">
<div class="inside">
<strong>Action </strong>
<br />               
<select id="post_action" name="post_action" style="width:100px;">
<option <?php if ($var1[$i]['post_action'] == "run_this_rule_now" ) echo 'selected' ; ?> value="run_this_rule_now">Run This Rule Now</option>
<option <?php if ($var1[$i]['post_action'] == "move_all_posts_to_trash" ) echo 'selected' ; ?> value="move_all_posts_to_trash">Move All Posts To Trash</option>
<option <?php if ($var1[$i]['post_action'] == "duplicate_this_rule" ) echo 'selected' ; ?> value="duplicate_this_rule">Duplicate This Rule</option>
<option <?php if ($var1[$i]['post_action'] == "permanently_delete_all_posts" ) echo 'selected' ; ?> value="permanently_delete_all_posts">Permanently Delete All Posts</option> 
<option <?php if ($var1[$i]['post_action'] == "delete_this_rule" ) echo 'selected' ; ?> value="delete_this_rule">Delete This Rule</option>

</select>    
</div>
</td> 

<input type="hidden" name="postid" value="<?php echo $var1[$i]['id']; ?>">
<td width="5%">
<div class="inside">

<br />               
<input type="submit" name="slm_search_btn2" class="button button-primary" value="Save Article" />
<br />            
</div>
</td>


</tr>
</table>
</form>
<?php 
}
?>


</div>

<?php
include_once( 'news_rule_class.php' ); //For rendering the license List Table
$license_list = new WPLM_List_Licenses();
if (isset($_REQUEST['action'])) { 
if (isset($_REQUEST['action'])) { 
$license_list->delete_licenses(strip_tags($_REQUEST['id']));
}
}

$license_list->prepare_items_list();

?>

</div></div>
</div>

<?php
}


